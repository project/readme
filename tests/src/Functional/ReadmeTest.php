<?php

namespace Drupal\Tests\readme\Functional;

use Drupal\Tests\BrowserTestBase;
use Michelf\Markdown;

/**
 * Tests readme conventions.
 *
 * @group Naming
 */
class ReadmeTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static array $modules = ['system', 'help', 'readme'];

  /**
   * Default Theme.
   *
   * @var string
   */
  protected string $defaultTheme = 'stark';

  /**
   * Tests Module enabled and on modules page access.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testModuleLoads() {
    $adminUser = $this->drupalCreateUser([
      'administer modules',
      'access administration pages',
    ]);
    $this->drupalLogin($adminUser);

    // Check for Readme link on 'Extend' page.
    $this->drupalGet('admin/modules');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->responseContains('<label id="module-readme" for="edit-modules-readme-enable" class="module-name table-filter-text-source">Readme</label>');
    $this->assertSession()
      ->responseContains('<span class="text module-description">Allows site builders and administrator to view a module\'s README file.</span>');
  }

  /**
   * Test Readme Index page loads.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testReadmeIndexPageLoads() {
    $adminUser = $this->drupalCreateUser([
    'administer modules',
    'access administration pages',
    ]);
    $this->drupalLogin($adminUser);

    // Check Readme main page.
    $this->drupalGet('admin/readme');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->responseContains('<title>Readme | Drupal</title>');
    $this->assertSession()
      ->responseContains('<dt class="list-group__link"><a href="/admin/readme/readme">Readme</a></dt>');
    $this->assertSession()
      ->responseContains('<dd class="list-group__description">Allows site builders and administrator to view a module\'s README file.</dd>');
  }

  /**
   * Test Admin page loads
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testReadmeMarkdownPageLoads() {
    $adminUser = $this->drupalCreateUser([
      'administer modules',
      'access administration pages',
    ]);
    $this->drupalLogin($adminUser);

    // Check Readme detail page for README.md.
    $this->drupalGet('admin/readme/readme');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('<title>Readme');
    $this->assertSession()->responseContains('Table of Contents');
  }

  /**
   * Test Markdown Child (README.txt) loads
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testReadmeTestPlainPageLoads() {
    $adminUser = $this->drupalCreateUser([
      'administer modules',
      'access administration pages',
    ]);
    $this->drupalLogin($adminUser);

    // Check Readme detail page for README . txt .
    $this->drupalGet('admin/readme/readme_test');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->responseContains('<title>Readme test');
    $this->assertSession()
      ->responseContains('<pre>One two one two, this is just a test -- Beastie Boys</pre>');
    $this->drupalLogout();
    }

  /**
   * Test Token Access.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testReadmeTokenAccess() {
    $adminUser = $this->drupalCreateUser([
      'administer modules',
      'access administration pages',
    ]);
    $this->drupalLogin($adminUser);
    // Check Readme HTML access allowed with valid token.
    $this->drupalGet('admin/readme/readme_test/html', [
      'query' => [
        'token' => \Drupal::state()->get('readme.token'),
      ],
    ]);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->responseContains('<pre>One two one two, this is just a test -- Beastie Boys</pre>');
  }

  /**
   * Test Anonymous 403 access Denied
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testReadmeAnonymousAccessDenied() {
    // Check Readme HTML access denied.
    $this->drupalGet('admin/readme/readme_test/html');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test Clear Token Access Denied.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testReadmeClearTokenAccessDenied() {
    $adminUser = $this->drupalCreateUser([
      'administer modules',
      'access administration pages',
    ]);
    $this->drupalLogin($adminUser);
    // Clear token value.
    $this->drupalGet('admin/config/development/readme/settings');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm(
      ['token' => ''],
      t('Save configuration'),
    );
    $this->drupalLogout();

    // Check Readme HTML access denied w/ token.
    $this->drupalGet('admin/readme/readme_test/html', [
      'query' => [
        'token' => \Drupal::state()->get('readme.token'),
      ],
    ]);
    $this->assertSession()->statusCodeEquals(403);
  }

}
